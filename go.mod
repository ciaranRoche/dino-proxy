module github.com/ciaranRoche/dino-proxy

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.7.0
	github.com/valyala/fasthttp v1.17.0 // indirect
)
