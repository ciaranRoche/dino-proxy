IMAGE_REG=quay.io
IMAGE_ORG=croche
IMAGE_NAME=dino-proxy
PROXY_DNS=internal-dino-internal-lb-1451291693.eu-west-1.elb.amazonaws.com

SHELL=/bin/bash

.PHONY: image/build
image/build:
	docker build -t $(IMAGE_REG)/$(IMAGE_ORG)/$(IMAGE_NAME) .

.PHONY: image/push
image/push: image/build
	docker push $(IMAGE_REG)/$(IMAGE_ORG)/$(IMAGE_NAME)
