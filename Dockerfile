FROM golang:1.15
ENV PROXY_DNS=xxxx
WORKDIR $GOPATH/src/github.com/ciaranRoche/dino-proxy
COPY . .
RUN go get -d -v ./...
RUN go install -v ./...
EXPOSE 8080
CMD ["dino-proxy"]