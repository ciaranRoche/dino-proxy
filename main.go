package main

import (
	"github.com/ciaranRoche/dino-proxy/pkg/router"
	"log"
	"net/http"
)

var PORT = ":8080"

func main() {
	r := router.Router()
	log.Fatal(http.ListenAndServe(PORT, r))
}