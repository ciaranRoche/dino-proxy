package handler

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
)

var _ RestHandler = dinoProxyHandler{}

type dinoProxyHandler struct {
	httpClient *http.Client
	log *logrus.Entry
}

type DinoProxyHandlerSpec struct {
	HttpClient *http.Client
	Logger *logrus.Entry
}

func NewDinoProxyHandler(spec *DinoProxyHandlerSpec) *dinoProxyHandler {
	return &dinoProxyHandler{
		httpClient: spec.HttpClient,
		log: spec.Logger.WithFields(logrus.Fields{"handler": "dino-proxy"}),
	}
}

func (d dinoProxyHandler) List(w http.ResponseWriter, r *http.Request) {
	d.log.Info("list proxy started")
	resp, err := d.httpClient.Get(getDNS())
	if err != nil {
		d.log.Errorf("failed to proxy list : %v", err)
	}
	io.Copy(w, resp.Body)
}

func (d dinoProxyHandler) Get(w http.ResponseWriter, r *http.Request) {
	d.log.Info("get proxy started")
	dns := fmt.Sprintf("%s/%s", getDNS(), mux.Vars(r)["id"])

	resp, err := d.httpClient.Get(dns)
	if err != nil {
		d.log.Errorf("failed to proxy list : %v", err)
	}
	io.Copy(w, resp.Body)
}

func (d dinoProxyHandler) Create(w http.ResponseWriter, r *http.Request) {
	d.log.Info("get proxy started")

	req, err := http.NewRequest("POST", getDNS(), r.Body)
	req.Header.Set("Content-Type", "application/json")

	resp, err := d.httpClient.Do(req)
	if err != nil {
		d.log.Errorf("failed to proxy list : %v", err)
	}
	io.Copy(w, resp.Body)
}

func (d dinoProxyHandler) Patch(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (d dinoProxyHandler) Delete(w http.ResponseWriter, r *http.Request) {
	d.log.Info("delete proxy started")
	dns := fmt.Sprintf("%s/%s", getDNS(), mux.Vars(r)["id"])
	resp, err := d.httpClient.Get(dns)
	if err != nil {
		d.log.Errorf("failed to proxy list : %v", err)
	}
	io.Copy(w, resp.Body)
}

func getDNS() string {
	return fmt.Sprintf("http://%s" ,os.Getenv("PROXY_DNS"))
}
