package router

import (
	"github.com/ciaranRoche/dino-proxy/pkg/handler"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
)

func Router() *mux.Router {
	r := mux.NewRouter()

	l := logrus.WithFields(logrus.Fields{"server": "dino-proxy"})
	l.Info("server starting")

	dinoProxyHandler := handler.NewDinoProxyHandler(&handler.DinoProxyHandlerSpec{
		HttpClient: &http.Client{},
		Logger: l,
	})

	r.HandleFunc("/", dinoProxyHandler.List).Methods(http.MethodGet)
	r.HandleFunc("/", dinoProxyHandler.Create).Methods(http.MethodPost)
	r.HandleFunc("/{id}", dinoProxyHandler.Get).Methods(http.MethodGet)
	r.HandleFunc("/{id}", dinoProxyHandler.Delete).Methods(http.MethodDelete)

	return r
}
